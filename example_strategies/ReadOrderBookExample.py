from strategy import *
from BaseStrategy import BaseStrategy


class MyStrategy(BaseStrategy):
    def __init__(self, strategy_actions):
        super().__init__(strategy_actions)

    def OnResetStrategyState(self):
        pass
        
    # Set flags before the strategy is started
    def SetFlags(self):
        pass

    def OnTrade(self, msg):
        
        #Simple Buying Example
        
        order_book = msg.instrument().aggregate_order_book()
        
        print("Best Bid " + str(order_book.BestBidLevel().price()) + " Worst Bid " + str(order_book.WorstBidLevel().price()))
        
        print("Best Ask " + str(order_book.BestAskLevel().price()) + " Worst Ask " + str(order_book.WorstAskLevel().price()))
        
        
        # Read order book 3 levels
        # Order book is 0 indexed
        
        print("Number of bid levels " + str(order_book.NumBidLevels()))
        
        for i in range(3): 
            print("Bid Level " + str(i) + ": " + str(order_book.BidSizeAtLevel(i)))
    
        
        pass


    def OnTopQuote(self, msg):
        pass
        
    def OnQuote(self, msg): 
        pass
    
    def OnBar(self, msg): 
        pass

    def OnDepth(self, msg):
        pass
    
    def OnResetStrategyState(self): 
        pass

    def OnOrderUpdate(self, msg): 
        
        print("on Order update")
        
        if (msg.completes_order()): 
            print("Order filled")
        
        
        print(msg.name())
            
        pass
    
    def OnResetStrategyState(self): 
        pass

    def RegisterForStrategyEvents(self, eventRegister, currDate):
        pass
    
    def OnNews(self, msg):
        pass  