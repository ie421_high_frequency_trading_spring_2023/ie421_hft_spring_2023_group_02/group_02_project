from strategy import *
from BaseStrategy import BaseStrategy


class MyStrategy(BaseStrategy):
    def __init__(self, strategy_actions):
        super().__init__(strategy_actions)

    def OnResetStrategyState(self):
        pass
        
    # Set flags before the strategy is started
    def SetFlags(self):
        pass

    def OnTrade(self, msg):
        
        #Simple Buying Example
        
        instrument = msg.instrument()
        
        trade_size = 100
        
        price = (instrument.top_quote().ask() + instrument.top_quote().bid()) / 2
        
        params = OrderParams(instrument, abs(trade_size), price, MARKET_CENTER_ID_IEX, ORDER_SIDE_BUY, ORDER_TIF_DAY, ORDER_TYPE_LIMIT)

        print("about to send new order for size " + str(trade_size) + " at $" + str(price) + " to " + "buy" + " " + instrument.symbol())
        
        self.trade_actions().SendNewOrder(params)
        
        pass


    def OnTopQuote(self, msg):
        pass
        
    def OnQuote(self, msg): 
        pass
    
    def OnBar(self, msg): 
        pass

    def OnDepth(self, msg):
        pass
    
    def OnResetStrategyState(self): 
        pass

    def OnOrderUpdate(self, msg): 
        
        print("on Order update")
        
        #check if order is filled
        if (msg.completes_order()): 
            print("Order filled")
        
        
        print(msg.name())
            
        pass
    
    def OnResetStrategyState(self): 
        pass

    def RegisterForStrategyEvents(self, eventRegister, currDate):
        pass
    
    def OnNews(self, msg):
        pass  